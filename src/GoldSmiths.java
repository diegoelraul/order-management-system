
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class GoldSmiths extends javax.swing.JPanel {

    /**
     * Creates new form GoldSmiths
     */
    GoldSmiths gs;
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    public static final int NOP = 0;
    public static final int INSERT = 1;
    public static final int UPDATE = 2;
    int operation;
    public GoldSmiths() {
        initComponents();
        this.gs = this;
        conn = MySQLConnect.connectDB();
        String sql = "SELECT name, number, contact_person, cp_number, email  FROM goldsmith";
        updateTableData(sql);
        disableAll();
        txtSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
    }
    
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"Owner Name","Number","Contact Person","Number", "Email"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private void enableAll(){
        txtGSID.setEnabled(true);
        txtON.setEnabled(true);
        txtONumber.setEnabled(true);
        txtCP.setEnabled(true);
        txtCPNumber.setEnabled(true);
        txtEmail.setEnabled(true);
    }
    
    private void disableAll(){
        txtGSID.setEnabled(false);
        txtON.setEnabled(false);
        txtONumber.setEnabled(false);
        txtCP.setEnabled(false);
        txtCPNumber.setEnabled(false);
        txtEmail.setEnabled(false);
    }
    
    private void searchFilter(){
        String key = txtSearch.getText();
        String sql;
        if(key.equals("")){
            sql = "SELECT name, number, contact_person, cp_number, email FROM goldsmith";
        }else{
            sql = "SELECT name, number, contact_person, cp_number, email FROM goldsmith WHERE ID like '%"+key+"%' OR name like '%"+key+"%'";
        }
        updateTableData(sql);
    }
    
    private void insertRecord(){
        String sql = "INSERT INTO goldsmith(name,number,contact_person,cp_number, email) VALUES (?, ?, ?, ?, ?)";
        String on = txtON.getText();
        String oNumber = txtONumber.getText();
        String cp = txtCP.getText();
        String cpNumber = txtCPNumber.getText();
        String email = txtEmail.getText();
        
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, on);
            ps.setString(2, oNumber);
            ps.setString(3, cp);
            ps.setString(4, cpNumber);
            ps.setString(5, email);
            
            ps.execute();
            clearFields();
            String loadTableQuery = "SELECT * FROM goldsmith";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
        }
    }
    
    private void updateRecord(){
        
        if(!"".equals(txtGSID.getText())){
            String sql = "update goldsmith set name = ?,number = ?,contact_person = ?,cp_number = ?, email = ? where ID = ?";
            String id = txtGSID.getText();
            String on = txtON.getText();
            String oNumber = txtONumber.getText();
            String cp = txtCP.getText();
            String cpNumber = txtCPNumber.getText();
            String email = txtEmail.getText();
               
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, on);
                ps.setString(2, oNumber);
                ps.setString(3, cp);
                ps.setString(4, cpNumber);
                ps.setString(5, email);
                ps.setString(6, id);
                

                ps.execute();
                clearFields();
                String loadTableQuery = "SELECT * FROM goldsmith";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
       }
    }
    
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tblGS.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }
//        finally{
//            try{
//                rs.close();
//                ps.close();
//            }catch(SQLException ex){
//                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
//            }
//        }
    }
    
    private void clearFields(){
        txtGSID.setText("");
        txtON.setText("");
        txtONumber.setText("");
        txtCP.setText("");
        txtCPNumber.setText("");
        txtEmail.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtGSID = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        pnlBase = new javax.swing.JPanel();
        pnlDetails = new javax.swing.JPanel();
        lblON = new javax.swing.JLabel();
        lblCP = new javax.swing.JLabel();
        lblONumber = new javax.swing.JLabel();
        lblCPNumber = new javax.swing.JLabel();
        txtON = new javax.swing.JTextField();
        txtCP = new javax.swing.JTextField();
        txtONumber = new javax.swing.JTextField();
        txtCPNumber = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        pnlTable = new javax.swing.JPanel();
        spTable = new javax.swing.JScrollPane();
        tblGS = new javax.swing.JTable();
        pnlButtons = new javax.swing.JPanel();
        lblSearch = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        btnNew = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();

        txtGSID.setFont(new java.awt.Font("Georgia", 0, 20)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        jLabel1.setText("GS ID");

        setBackground(new java.awt.Color(255, 255, 255));

        pnlBase.setBackground(new java.awt.Color(255, 255, 255));
        pnlBase.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Goldsmiths", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        pnlDetails.setBackground(new java.awt.Color(255, 255, 255));
        pnlDetails.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblON.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        lblON.setText("Owner Name");

        lblCP.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        lblCP.setText("Contact Person");

        lblONumber.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        lblONumber.setText("Mobile Number");

        lblCPNumber.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        lblCPNumber.setText("CP Mobile Number");

        txtON.setFont(new java.awt.Font("Georgia", 0, 20)); // NOI18N

        txtCP.setFont(new java.awt.Font("Georgia", 0, 20)); // NOI18N

        txtONumber.setFont(new java.awt.Font("Georgia", 0, 20)); // NOI18N

        txtCPNumber.setFont(new java.awt.Font("Georgia", 0, 20)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        jLabel2.setText("Email");

        txtEmail.setFont(new java.awt.Font("Georgia", 0, 20)); // NOI18N

        javax.swing.GroupLayout pnlDetailsLayout = new javax.swing.GroupLayout(pnlDetails);
        pnlDetails.setLayout(pnlDetailsLayout);
        pnlDetailsLayout.setHorizontalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetailsLayout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCP)
                    .addComponent(lblON))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtON, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCP, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCPNumber)
                    .addComponent(lblONumber))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtONumber, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(txtCPNumber))
                .addGap(43, 43, 43))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(306, 306, 306))
        );
        pnlDetailsLayout.setVerticalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetailsLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblON)
                    .addComponent(txtON, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblONumber)
                    .addComponent(txtONumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCP)
                    .addComponent(txtCP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCPNumber)
                    .addComponent(txtCPNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlTable.setBackground(new java.awt.Color(255, 255, 255));
        pnlTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tblGS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "GS ID", "Owner Name", "Number", "Contact Person", "Number"
            }
        ));
        tblGS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblGSMouseClicked(evt);
            }
        });
        spTable.setViewportView(tblGS);

        javax.swing.GroupLayout pnlTableLayout = new javax.swing.GroupLayout(pnlTable);
        pnlTable.setLayout(pnlTableLayout);
        pnlTableLayout.setHorizontalGroup(
            pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spTable)
                .addContainerGap())
        );
        pnlTableLayout.setVerticalGroup(
            pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spTable, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlButtons.setBackground(new java.awt.Color(255, 255, 255));
        pnlButtons.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblSearch.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        lblSearch.setText("Search");

        txtSearch.setFont(new java.awt.Font("Georgia", 0, 20)); // NOI18N

        btnNew.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        btnUpdate.setText("Edit");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Georgia", 1, 20)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlButtonsLayout = new javax.swing.GroupLayout(pnlButtons);
        pnlButtons.setLayout(pnlButtonsLayout);
        pnlButtonsLayout.setHorizontalGroup(
            pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnNew)
                .addGap(18, 18, 18)
                .addComponent(btnSave)
                .addGap(18, 18, 18)
                .addComponent(btnUpdate)
                .addGap(18, 18, 18)
                .addComponent(btnDelete)
                .addGap(18, 18, 18)
                .addComponent(btnClear)
                .addGap(64, 64, 64))
        );
        pnlButtonsLayout.setVerticalGroup(
            pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSearch)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNew)
                    .addComponent(btnSave)
                    .addComponent(btnUpdate)
                    .addComponent(btnDelete)
                    .addComponent(btnClear))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlBaseLayout = new javax.swing.GroupLayout(pnlBase);
        pnlBase.setLayout(pnlBaseLayout);
        pnlBaseLayout.setHorizontalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlButtons, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlBaseLayout.setVerticalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBaseLayout.createSequentialGroup()
                .addComponent(pnlDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        if(operation == INSERT){
            insertRecord();
        }else if(operation == UPDATE){
            updateRecord();
        }
        operation = NOP;
        btnClear.setEnabled(false);
        btnDelete.setEnabled(false);
        btnNew.setEnabled(true);
        btnSave.setEnabled(false);
        btnUpdate.setEnabled(false);
    }//GEN-LAST:event_btnSaveActionPerformed

    private void tblGSMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblGSMouseClicked
        // TODO add your handling code here:
        int selectedRow = tblGS.getSelectedRow();
        String selectedSID = tblGS.getModel().getValueAt(selectedRow, 0).toString();
        try{
            String sql = "SELECT * FROM goldsmith WHERE ID = '" + selectedSID + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            if(rs.next()){
                txtGSID.setText(rs.getString("ID"));
                txtON.setText(rs.getString("name"));
                txtONumber.setText(rs.getString("number"));
                txtCP.setText(rs.getString("contact_person"));
                txtCPNumber.setText(rs.getString("cp_number"));
                txtEmail.setText(rs.getString("email"));
              
                disableAll();
                btnUpdate.setEnabled(true);
                btnDelete.setEnabled(true);
                btnNew.setEnabled(false);
                btnSave.setEnabled(false);
                btnClear.setEnabled(false);              
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error : " + e);
        }
    }//GEN-LAST:event_tblGSMouseClicked

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        clearFields();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        int ans = JOptionPane.showConfirmDialog(this, "Are you sure you want to delete this record?", "Delete!", JOptionPane.YES_NO_OPTION);
        if(ans == JOptionPane.YES_OPTION){
            String sql = "DELETE FROM goldsmith WHERE ID = ?";
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, txtGSID.getText());
                ps.execute();
                sql = "SELECT * FROM goldsmith";
                updateTableData(sql);
                clearFields();
                btnNew.setEnabled(true);
                btnDelete.setEnabled(false);
                btnClear.setEnabled(false);
                btnUpdate.setEnabled(false);
            }catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error While Deleting this record!");
            }
        }else if(ans == JOptionPane.NO_OPTION){

        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        operation = UPDATE;
        enableAll();
        txtGSID.setEnabled(false);
        btnNew.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnSave.setEnabled(true);
        btnClear.setEnabled(false);
        btnDelete.setEnabled(false);
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
        clearFields();
        enableAll();
        txtGSID.setEnabled(false);
        btnNew.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        btnSave.setEnabled(true);
        btnClear.setEnabled(true);
        operation = INSERT;
    }//GEN-LAST:event_btnNewActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblCP;
    private javax.swing.JLabel lblCPNumber;
    private javax.swing.JLabel lblON;
    private javax.swing.JLabel lblONumber;
    private javax.swing.JLabel lblSearch;
    private javax.swing.JPanel pnlBase;
    private javax.swing.JPanel pnlButtons;
    private javax.swing.JPanel pnlDetails;
    private javax.swing.JPanel pnlTable;
    private javax.swing.JScrollPane spTable;
    private javax.swing.JTable tblGS;
    private javax.swing.JTextField txtCP;
    private javax.swing.JTextField txtCPNumber;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtGSID;
    private javax.swing.JTextField txtON;
    private javax.swing.JTextField txtONumber;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
