
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class PendingReport extends javax.swing.JPanel {

    /**
     * Creates new form PendingReport
     */
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    PendingReport pr;
    String sent = "No";
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
    LocalDateTime now = LocalDateTime.now(); 
    public PendingReport() {
        initComponents();
        this.pr = this;
        conn = MySQLConnect.connectDB();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
        LocalDateTime now = LocalDateTime.now();  
//        System.out.println(dtf.format(now));"
        String sql = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id,customer_order.receive_date,customer_order.delivery_date, gs_order.gs_name, gs_order.given_date,gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id WHERE customer_order.delivery_date < '" + dtf.format(now) + "'";
        updateTableData(sql);
        String sql1 = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id,customer_order.receive_date,customer_order.delivery_date, gs_order.gs_name, gs_order.given_date,gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id WHERE customer_order.delivery_date > '" + dtf.format(now) + "'";
        updateTableData1(sql1);
        txtSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
        txtSearch1.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter1();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter1();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
    }
    
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"Customer Name","Order ID","MJ ID","Customer Receive Date","Customer Delivery Date","Goldsmith Name","Given Date", "Receive Date", "Received"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private DefaultTableModel getMyTableModel1 (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"Customer Name","Order ID","MJ ID","Customer Receive Date","Customer Delivery Date","Goldsmith Name","Given Date", "Receive Date", "Received"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private void searchFilter(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
        LocalDateTime now = LocalDateTime.now();  
        String key = txtSearch.getText();
        String sql;
        if(key.equals("")){
            sql = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id,customer_order.receive_date, customer_order.delivery_date, gs_order.gs_name, gs_order.given_date,gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id WHERE customer_order.delivery_date < '" + dtf.format(now) + "'";
        }else{
            sql = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id,customer_order.receive_date, customer_order.delivery_date, gs_order.gs_name, gs_order.given_date,gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id where customer_order.mj_id like '%"+key+"%' OR customer_order.customer_id like '%"+key+"%' OR customer_order.order_id like '%"+key+"%' AND customer_order.delivery_date < '" + dtf.format(now) + "'";
        }
        updateTableData(sql);
    }
    
    private void searchFilter1(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
        LocalDateTime now = LocalDateTime.now();  
        String key = txtSearch1.getText();
        String sql;
        if(key.equals("")){
            sql = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id,customer_order.receive_date, customer_order.delivery_date, gs_order.gs_name, gs_order.given_date,gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id WHERE customer_order.delivery_date > '" + dtf.format(now) + "'";
        }else{
            sql = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id,customer_order.receive_date, customer_order.delivery_date, gs_order.gs_name, gs_order.given_date,gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id where customer_order.mj_id like '%"+key+"%' OR customer_order.customer_id like '%"+key+"%' OR customer_order.order_id like '%"+key+"%' AND customer_order.delivery_date > '" + dtf.format(now) + "'";
        }
        updateTableData1(sql);
    }
    
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tblPR.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }
//        finally{
//            try{
//                rs.close();
//                ps.close();
//            }
//              catch(SQLException ex){
//                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
//            }
//        }
    }
    
    private void updateTableData1(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tblPR1.setModel(getMyTableModel1(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }
//        finally{
//            try{
//                rs.close();
//                ps.close();
//            }
//              catch(SQLException ex){
//                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
//            }
//        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBase = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPR = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtSearch1 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPR1 = new javax.swing.JTable();
        btnPrint = new javax.swing.JButton();
        btnPrint1 = new javax.swing.JButton();

        pnlBase.setBackground(new java.awt.Color(255, 255, 255));
        pnlBase.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Customer Reports", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel1.setText("Search");

        txtSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        tblPR.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblPR);

        jLabel2.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel2.setText("Search");

        txtSearch1.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        tblPR1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblPR1);

        btnPrint.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnPrint1.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnPrint1.setText("Print");
        btnPrint1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrint1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBaseLayout = new javax.swing.GroupLayout(pnlBase);
        pnlBase.setLayout(pnlBaseLayout);
        pnlBaseLayout.setHorizontalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBaseLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSearch))
            .addComponent(jScrollPane1)
            .addGroup(pnlBaseLayout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSearch1))
            .addComponent(jScrollPane2)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBaseLayout.createSequentialGroup()
                .addContainerGap(424, Short.MAX_VALUE)
                .addGroup(pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBaseLayout.createSequentialGroup()
                        .addComponent(btnPrint)
                        .addGap(408, 408, 408))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBaseLayout.createSequentialGroup()
                        .addComponent(btnPrint1)
                        .addGap(409, 409, 409))))
        );
        pnlBaseLayout.setVerticalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBaseLayout.createSequentialGroup()
                .addGroup(pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrint)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtSearch1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrint1))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 924, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 840, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlBase, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // TODO add your handling code here:
        MessageFormat header =  new MessageFormat("Pending Customer Order Report Date : " + dtf.format(now));
        MessageFormat footer =  new MessageFormat("Page{0, number, integer}");
        try{
            tblPR.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        }catch(Exception e){
            System.err.format("Cannot print %s%n", e.getMessage());
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnPrint1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrint1ActionPerformed
        // TODO add your handling code here:
        MessageFormat header =  new MessageFormat("Future Customer Order Report Date : " + dtf.format(now));
        MessageFormat footer =  new MessageFormat("Page{0, number, integer}");
        try{
            tblPR1.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        }catch(Exception e){
            System.err.format("Cannot print %s%n", e.getMessage());
        }
    }//GEN-LAST:event_btnPrint1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnPrint1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlBase;
    private javax.swing.JTable tblPR;
    private javax.swing.JTable tblPR1;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtSearch1;
    // End of variables declaration//GEN-END:variables
}
